# -*- coding: utf-8 -*-
#Modulo funcones math

def sumainferior(f, a, b, n):
    # Calcula la longitud de cada subintervalo
    delta_x = (b - a) / n
    
    # Inicializa la suma
    sum = 0
    
    # Aproxima la integral en cada subintervalo
    for i in range(n):
        # Calcula el límite inferior y superior del subintervalo
        x_inferior = a + i * delta_x
        x_superior = a + (i + 1) * delta_x
        
        # Aproxima la integral en el subintervalo como el área del rectángulo
        # con base delta_x y altura igual al valor mínimo de f(x) en el subintervalo
        sum += f(x_inferior) * delta_x
    
    return sum


def trapecio(a,b,n):
    # Se delaramos la variable h
    Δx = (b - a) / n
    
    # Se declara la variable para formular la suma
    integral = f(a) + f(b)
    
    for i in range(1,n):
        k = a + i*Δx
        integral = integral + 2 * f(k)
    
    # Se encuentra el valor de integración final
    integral = integral * Δx/2
    
    return integral

def ssuperiores(a, b, n):
    h = (b - a) / n
    suma = 0
    for i in range(n):
        xi = a + (i + 1) * h
        suma += funcion(xi)
    return h * suma


def integral_derecha(f, a, b, n):
    dx = (b - a) / n
    suma = 0
    for i in range(n):
        xi = a + (i + 1) * dx
        suma += f(xi)
    integral = dx * suma
    return integral

def sumaizquierda (f, a, b, n):
    # Calcula la longitud de cada subintervalo
    delta_x = (b - a) / n
    
    # Inicializa la suma
    sum = 0
    
    # Aproxima la integral en cada subintervalo
    for i in range(n):
        # Calcula el límite inferior del subintervalo
        x_inferior = a + i * delta_x
        
        # Aproxima la integral en el subintervalo como el área del rectángulo
        # con base delta_x y altura igual al valor de f(x) en el límite inferior
        sum += f(x_inferior) * delta_x
    
    return sum