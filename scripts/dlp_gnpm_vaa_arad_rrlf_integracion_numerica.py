# -*- coding: utf-8 -*-
#%% Sumas inferiores Duran Lopez Pablo
"""
Descripcion:
    Hacer un programa que calcule mediante sumas inferiores la integralde x**3 -4x**2 +4x en el
    intervalo [0,3]
 Delimitacion:
     Se calcula la integral en el intervalo [0,3] para una n arbitraria que desee el usuario
Solucion:
    Se elige una n arbitraria que sea unentero el cual los intervalos se van a dividir entre n, siendo dx=3/n la longitud de cada intervalo
    despues se procede a tomar el valor de x mas chico para cada intrvalo y se evalua la funcion en esa, siendo esto para cada ubintervalo multiplicado
    por la longitud del intervalo, ejecutar la suma asi hasta n, el cual sera el valor de la integral
    
Ejemplo:
    Pregunta 1:Inserta la cantidad de subintervalos n para la integral, donde n es mayor igual que 1. 
   Respuesta: 100
    
   La aproximación mediante sumas inferiores de la integral de f(x) en el intervalo 
   [0, 3] con n = 100 subintervalos es: 2.2052249999999995
"""

#Definimos la funcion suma inferior
def sumainferior(f, a, b, n):
    # Calcula la longitud de cada subintervalo
    delta_x = (b - a) / n
    
    # Inicializa la suma
    sum = 0
    
    # Aproxima la integral en cada subintervalo
    for i in range(n):
        # Calcula el límite inferior y superior del subintervalo
        x_inferior = a + i * delta_x
        x_superior = a + (i + 1) * delta_x
        
        # Aproxima la integral en el subintervalo como el área del rectángulo
        # con base delta_x y altura igual al valor mínimo de f(x) en el subintervalo
        sum += f(x_inferior) * delta_x
    
    return sum

# Define la función f(x)
def f(x):
    return x**3 - 4*x**2 + 4*x

# Aproxima la integral de f(x) en el intervalo [0, 3] con n subintervalos
n= int(input("Inserta la cantidad de subintervalos n para la integral, donde n es mayor igual que 1. "))
integral = sumainferior(f, 0, 3, n)
#Muestra el resultado
print("La aproximación mediante sumas inferiores de la integral de f(x) en el intervalo [0, 3] con n = {} subintervalos es: {}" .format(n, integral))

"""El ejercicio fue facil de realizar con los elemtos vistos en clase y como la teoria ya era
de mi conocimiento fue facil de realizar, no represento mayor dificultad """

#%% (Reyes Ramírez Luis Fernando)

"""
Descripción

Escribir un codigo para calcular la integral de una función en un intervalo mediante 
la regla del trapecio, y mostrar el resultado.

Delimitación 

Se partirá el intervalo (a,b) en n subintervalos, donde n es un número entero 
positivo proporcionado por el usuario. Despues, se calculará el área de los
rectángulos de base de cada subintervalo y se utilizará la altura del extremo 
del subintervalo para estimar el valor de la función. Posteriormente, 
se sumará el área total de los rectángulos. Finalmente, se mostrará el resultado.

Solución

Se calcula la longitud de cada subintervalo dividiendo la longitud
total del intervalo (b-a) entre el número de subintervalos n. Luego, se 
encontrarán los puntos extremos de cada subintervalo sumando la longitud de cada
subintervalo al punto de inicio a para cada i-ésimo subintervalo. Después se
calcula el valor de la función en el límite Con uncho de cada subintervalo y se 
multiplica por la longitud de cada subintervalo para obtener el área de cada 
rectángulo, hay que tener en cuenta que para la regla del trapecio 
todas las funciones se multiplican por 2, a excepción de la primera
.Finalmente se sumarán todas las áreas de los rectángulos y se 
mostrará el resultado final.

Ejemplos

Con un limite inferior de integración: 0
Con un limite superior de integración: 3
Con un numero de subintervalos: 5

Se sigue que, el valor de la integral x³ -4x² + 4x en el intervalo [0,3] con 
n=5 por la regla del trapecio es : 2.3400

Nota. 
Como vemos 2.34 es un valor relativamente cercano a 2.25   

Con un limite inferior de integración: 0
Con un limite superior de integración: 3
Con un numero de subintervalos: 1000

Se sigue que, el valor de la integral x³ -4x² + 4x en el intervalo [0,3] con 
n=5 por la regla del trapecio es : 2.25
"""
# Regla del trapecio

# Definimos la funcion a integrar
def f(x):
    return x**3 - 4*x**2 + 4*x

# Puesta en marcha del metodo trapecio
def trapecio(a,b,n):
    # Se delaramos la variable h
    Δx = (b - a) / n
    
    # Se declara la variable para formular la suma
    integral = f(a) + f(b)
    
    for i in range(1,n):
        k = a + i*Δx
        integral = integral + 2 * f(k)
    
    # Se encuentra el valor de integración final
    integral = integral * Δx/2
    
    return integral
    
# Datos de entrada 
# limite inferior de integracion
lim_inf = 0
# limite superior de integracion
lim_sup = 3
# numero de subintervalos
num_sub = int(input("Inserta el numero de intervalos" ))

# Se manda a llamar al método trapecio() 
resultado = trapecio(lim_inf, lim_sup, num_sub)
# Se imprime el valor final  
print(f"El valor de la integral x³ -4x² + 4x en el intervalo [0,3] con n=5 por la regla del trapecio es : %.2f" % resultado)
"""
Comentario final 

Siento que para este método de integración lo mas dificil fue saber cómo es 
que funciona la regla del trapecio, así pues, me costó recordar cómo es 
que trabaja una integral definida, aunque, creo que con lo que se dio en clase, 
fue mas que suficiente para trabajar con la implementación. Por último, me 
había confundio mucho en un principio que es lo que en se tenía que desarrolar en 
papel y lapiz.

Adicionalmente, estos recursos/referencias me ayudo para facilitar el código:

Integración numérica, CoCalc, consultado el 03 05 2023,
recuperado de 
https://cocalc.com/share/public_paths/embed/97db6047a5f094ad6d371128e4351fb1a3c6572c

Trapezoidal Rule for Approximate Value of Definite Integral, geeksforgeeks, consultado el 03 05 2023,
recuperado de 
https://www.geeksforgeeks.org/trapezoidal-rule-for-approximate-value-of-definite-integral/

Implementing The Trapezium Rule With Python, Matheus Gomes, consultado el 03 05 2023,
recuperado de 
https://matgomes.com/trapezium-rule-with-python/

Trapezoidal Rule Calculator for a Function, eMathHelp, consultado el 04 05 2023,
recuperado de 
https://www.emathhelp.net/calculators/calculus-2/trapezoidal-rule-calculator/"""

#%% Sumas superiores
""" #Albiter Reyes Alzak Daniel

Definición: 
Escribir un programa que calcule la integral por el metodo de sumas superiores de una funcion, 
en un intervalo determinado y muestre el resultado

Delimitación: 
Dependiendo de la n proporcionada por el usuario , partir el intervalo (a,b) en n sub-intervalos, calcular
el área de los rectangulos de base de cada sub-intervalo y evaluar la funcion en el punto en su 
extrema derecha del sub-intervalo con el que estemos trabajando, 
sumar el area total e imprimir el resultado

Solucion:
Definir la función a calcular para posteriormente paritr los intervalos dependiendo de la 'n' proporcionada, 
despues calcular el area del rectangulo de base (b-a)/n y altura f(i_n) donde i_n es el extremo derecho del 
sub-intervalo.
Finalmente se realiza la iteracion para todos los i's de 0 a n y se suman los resultados y se imprime el resultado.

Ejemplo:
a=0
b=3
n=5

El resultado de las sumas superiores es 3.24

"""


def funcion(x):
    
    return x**3 - 4*x**2 + 4*x

#a es el extremo izquierdo de mi intervalo
#b es el extremo derecho de mi intervalo
#n es el  numero de subintervalos a considerar;nota:Entre mas subintervalos, la respuesta será más precisa.

def ssuperiores(a, b, n):
    h = (b - a) / n
    suma = 0
    for i in range(n):
        xi = a + (i + 1) * h
        suma += funcion(xi)
    return h * suma

a = 0
b = 3
n = int(input("Inserta el numero de intervalos" ))

resultado = ssuperiores(a, b, n)
print(f"EL resultado de las sumas superiores es {resultado:.7f}")
"""
El problema en realidad no era complicao por que ya sabia como realizar las sumas superiores y el proceso atrás de,
lo complicado fue como definir la funcion para que la computadora hiciera las iteraciones correspondientes, pero
entre todo el equipo nos fue posible realizarlo de una buena manera.
"""

# %% (Gutiérrez Nava Pablo Miguel)
"""

Definición

Escribir un codigo para calcular la integral de una función en un intervalo dado
utilizando el método de sumas por la derecha, y mostrar el resultado.

Delimitación

Se partirá el intervalo (a,b) en n subintervalos, donde n es un número entero 
positivo proporcionado por el usuario. Despues, se calculará el área de los
rectángulos de base de cada subintervalo y se utilizará la altura del extremo 
derecho del subintervalo para estimar el valor de la función. Posteriormente, 
se sumará el área total de los rectángulos. Por ultimo, se mostrará el resultado.

Solución

Se calcula la longitud de cada subintervalo dividiendo la longitud
total del intervalo (b-a) entre el número de subintervalos n. Luego, se 
encontrarán los puntos extremos de cada subintervalo sumando la longitud de cada
subintervalo al punto de inicio a para cada i-ésimo subintervalo. Despué se
calcula el valor de la función en el límite derecho de cada subintervalo y se 
multiplica por la longitud de cada subintervalo para obtener el área de cada 
rectángulo. Finalmente se sumarán todas las áreas de los rectángulos y se 
mostrará el resultado final.

Ejemplo

El valor de la integral x³ -4x² + 4x en el intervalo [0,3] con n=5 por el metodo
de sumas por la derecha es: 3.239999999999999

"""

# comenzamos a definir nuestras funciones y variables para el procedimiento
def integral_derecha(f, a, b, n):
    dx = (b - a) / n
    suma = 0
    for i in range(n):
        xi = a + (i + 1) * dx
        suma += f(xi)
    integral = dx * suma
    return integral

# aqui definimos nuestra función a integrar
def f(x):
    return x**3 - 4*x**2 + 4*x

# aqui colocamos nuestros limites de integración
a = 0
b = 3

# calculamos la integral por el método de sumas por la derecha con n intervalos
n= int(input("Inserta la cantidad de intervalos n, donde n es un numero natural" ))
respuesta = integral_derecha(f, a, b, n)

print("El valor de la integral x³ -4x² + 4x en el intervalo [0,3] con n= {} por el metodo de sumas por la derecha es:{}".format(n, respuesta))

"""En general, fue una tarea interesante que me permitió aplicar los conocimientos teóricos en un contexto práctico y 
lograr un resultado concreto. La creación de este código me permitió mejorar mis habilidades de resolución de problemas y
 familiarizarme con los métodos numéricos utilizados para la aproximación de integrales en este caso usando el método de sumas 
 por la derecha. No estuvo complicado, fue divertido. 
"""
#%%

"""Definicion: 
Escribir un programa que calcule la integral por el metodo de sumas por 
izquierda de una funcion, en un intervalo determinado y muestre el resultado

Delimitacion: 
Dependiendo de la n proporcionada, partir el intervalo (a,b) en n sub-intervalos, calcular
el area de los rectangulos de base de cada sub-intervalo y la altura que tenga el extremo izquierdo, 
sumar el area total e imprimir el resultado

Solucion:
Cortar los intervalos dependiendo de la 'n' proporcionada, despues calcular 
el area del rectangulo de base (b-a)/n y altura f(i_n) donde i_n es el extremo izquiero del 
intervalo y se calcula como a+(b-a)/n*i donde i es un iterador que va de 0 a n
Finalmente se realiza la iteracion para todos los i's de 0 a n y se suman los resultados
Ejemplo: 

f(x) = x^3-4x^2+4x
n = 5
a = 0
b = 3
Resultado = 3.24
"""
def sumaizquierda (f, a, b, n):
    # Calcula la longitud de cada subintervalo
    delta_x = (b - a) / n
    
    # Inicializa la suma
    sum = 0
    
    # Aproxima la integral en cada subintervalo
    for i in range(n):
        # Calcula el límite inferior del subintervalo
        x_inferior = a + i * delta_x
        
        # Aproxima la integral en el subintervalo como el área del rectángulo
        # con base delta_x y altura igual al valor de f(x) en el límite inferior
        sum += f(x_inferior) * delta_x
    
    return sum

# Define la función f(x)
def f(x):
    return x**3 - 4*x**2 + 4*x

# Aproxima la integral de f(x) en el intervalo [0, 3] con n subintervalos
n= int(input("Inserta la cantidad de intervalos n, donde n es un numero natural" ))
integral = sumaizquierda(f, 0, 3, n)

print("La aproximación mediante sumas por la izquierda de la integral de f(x) en el intervalo [0, 3] con n = {} subintervalos es:{}".format(n, integral))

"""Me pareció fácil de implementar el problema, ya conociendo el tema desde la teoría."""