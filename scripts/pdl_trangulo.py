# -*- coding: utf-8 -*-
"""
Created on Mon Mar 13 18:45:07 2023

@author: Duran Lopez Pablo
"""
"""
Definicion:
    Dada las longitudes de 3 segmentos determinar si se puede formar un triangulo con ellos

Delimitacion:
    Se limita solo a decir si es cierto o falso el que se pueda formar un triangulo 
    con los valores de los 3 segmentos que el usuario inserte

Solucion:
    Se solicita al usuario que ingrese las longitudes de los 3 segmentos, sin importar el orden
    seguido de eso, se recopila la informacion para despues procesar la informacion mediante un discrimante
    definido como "x+y>z, x+z>y y z+y> x" si se cumple dicha condicion el programa lanzara un mensaje 
    que indique que si se puede formar un triangulo, en caso contrario se indicara que no
    es posible formar un triangulo.

Ejemplo:
    *En caso negativo*
    
    Pregunta 1: Ingrese la longitud del primer segmento: 
        Respuesta 1: 4

    Pregunta 2: Ingrese la longitud del segundo segmento: 
        Respuesta 2: 3

    Pregunta 3: Ingrese la longitud del tercer segmento:
        Respuesta 3: 12
    No es posible formar un triángulo con los lados 4.0 , 3.0 y 12.0
    
    *En caso afirmativo*
    
    Pregunta 1: Ingrese la longitud del primer segmento: 
        Respuesta 1: 5

    Pregunta 2: Ingrese la longitud del segundo segmento: 
        Respuesta 2: 6

    Pregunta 3: Ingrese la longitud del tercer segmento:
        Respuesta 3: 4

Es posible formar un triángulo con los lados 5.0 , 6.0 y 4.0
"""

#Implementacion

def triangulo(a, b, c):
    if (a + b > c) and (a + c > b) and (b + c > a):
        return True
    else:
        return False

# El usuario ingresa los valores de los 3 segmentos
a = float(input("Ingrese la longitud del primer segmento: "))
b = float(input("Ingrese la longitud del segundo segmento: "))
c = float(input("Ingrese la longitud del tercer segmento: "))

# Determinar si es posible formar un triángulo con los valores ingresados
if triangulo(a, b, c):
    print("Es posible formar un triángulo con los lados de longitud", a, ",", b, "y", c)
else:
    print("No es posible formar un triángulo con los lados de longitud", a, ",", b, "y", c)
    
    
"""El problema fue facil de resolver con los elementos vistos en clase fue mas que suficiente
y norequirio de investigacion adicional, pues el problema a resolver ya era de conocimientomio
entonces, fue un problema sencillo y rapido"""
