# -*- coding: utf-8 -*-
"""
Created on Fri May  5  2023

@author: Duran Lopez Pablo
SUMAS INFERIORES

Descripcion:
    Hacer un programa que calcule mediante sumas inferiores la integralde x**3 -4x**2 +4x en el
    intervalo [0,3]
 Delimitacion:
     Se calcula la integral en el intervalo [0,3] para una n arbitraria que desee el usuario
Solucion:
    Se elige una n arbitraria que sea unentero el cual los intervalos se van a dividir entre n, siendo dx=3/n la longitud de cada intervalo
    despues se procede a tomar el valor de x mas chico para cada intrvalo y se evalua la funcion en esa, siendo esto para cada ubintervalo multiplicado
    por la longitud del intervalo, ejecutar la suma asi hasta n, el cual sera el valor de la integral
    
Ejemplo:
    Pregunta 1:Inserta la cantidad de subintervalos n para la integral, donde n es mayor igual que 1. 
   Respuesta: 100
    
   La aproximación mediante sumas inferiores de la integral de f(x) en el intervalo 
   [0, 3] con n = 100 subintervalos es: 2.2052249999999995
"""

#Definimos la funcion suma inferior
def sumainferior(f, a, b, n):
    # Calcula la longitud de cada subintervalo
    delta_x = (b - a) / n
    
    # Inicializa la suma
    sum = 0
    
    # Aproxima la integral en cada subintervalo
    for i in range(n):
        # Calcula el límite inferior y superior del subintervalo
        x_inferior = a + i * delta_x
        x_superior = a + (i + 1) * delta_x
        
        # Aproxima la integral en el subintervalo como el área del rectángulo
        # con base delta_x y altura igual al valor mínimo de f(x) en el subintervalo
        sum += f(x_inferior) * delta_x
    
    return sum

# Define la función f(x)
def f(x):
    return x**3 - 4*x**2 + 4*x

# Aproxima la integral de f(x) en el intervalo [0, 3] con n subintervalos
n= int(input("Inserta la cantidad de subintervalos n para la integral, donde n es mayor igual que 1. "))
integral = sumainferior(f, 0, 3, n)
#Muestra el resultado
print("La aproximación mediante sumas inferiores de la integral de f(x) en el intervalo [0, 3] con n = {} subintervalos es: {}" .format(n, integral))

"""El ejercicio fue facil de realizar con los elemtos vistos en clase y como la teoria ya era
de mi conocimiento fue facil de realizar, no represento mayor dificultad """